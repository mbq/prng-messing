#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

uint64_t rol64(uint64_t x, int k){
 return (x << k) | (x >> (64 - k));
}

struct xoshiro256ss_state {
	uint64_t s[4];
} j;

uint64_t xoshiro256ss(struct xoshiro256ss_state *state){
	uint64_t *s = state->s;
	uint64_t const result = rol64(s[1] * 5, 7) * 9;
	uint64_t const t = s[1] << 17;

	s[2] ^= s[0];
	s[3] ^= s[1];
	s[1] ^= s[2];
	s[0] ^= s[3];

	s[2] ^= t;
	s[3] = rol64(s[3], 45);

	return result;
}


int main(int argc,char **argv){
 FILE *const out=fdopen(dup(fileno(stdout)), "wb");
 j.s[0]=1;
 j.s[1]=2;
 j.s[2]=3;
 j.s[3]=4;
 while(1){

  uint32_t v=xoshiro256ss(&j);
  fwrite(&v,sizeof(v),1,out);
 }
 fclose(out);
}
