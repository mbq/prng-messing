#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

typedef uint64_t fnv_t;

static inline fnv_t new_fnv(){
 return 0xcbf29ce484222325;
}

#define k 0xc58efd154ce32f6d

static inline uint32_t get_hash(fnv_t *x){
 uint32_t rot=x[0]>>59;
 uint32_t s=((x[0]>>18)^(x[0]))>>27;
 return (s<<((-rot)&31))|(s>>rot);
 //return x[0];
}
static inline uint64_t get_state(fnv_t *x){
 return x[0];
}

static inline uint64_t soak8(fnv_t *x,size_t len,char *v){
 for(size_t e=0;e<len;e++){
  x[0]=x[0]^((uint64_t)(v[e]));
  x[0]=x[0]*0x100000001b3;
 }
}

static inline uint64_t soak8_pcg(fnv_t *x,size_t len,char *v){
 for(size_t e=0;e<len;e++){
  x[0]=x[0]^((uint64_t)(v[e]));
  //x[0]=x[0]*0x100000001b3;
  x[0]=x[0]*6364136223846793005+1;
 }
}

static inline uint64_t soak8_pcg_alt(fnv_t *x,size_t len,char *v){
 for(size_t e=0;e<len;e++){
  x[0]=x[0]*6364136223846793005+(v[e]|1);
 }
}

static inline uint64_t soak64_pcg(fnv_t *x,uint64_t in){
  x[0]=x[0]^in;
  //x[0]=x[0]*0x100000001b3;
  x[0]=x[0]*6364136223846793005+1;
}

static inline uint64_t soak64_pcg_alt(fnv_t *x,uint64_t in){
  //x[0]=x[0]*0x100000001b3;
  x[0]=x[0]*6364136223846793005+(in|1);
}




int main(int argc,char **argv){
 FILE *const out=fdopen(dup(fileno(stdout)), "wb");
 int64_t e=0;
 while(1){
  fnv_t g=new_fnv();
//  uint64_t ee=((e+1)*k+1)*k; //passes, probably better...
  uint64_t ee=e*k; //passes! +-Weyl trick, generates more 1s around...
 // soak8_pcg(&g,sizeof(ee),(char*)&ee); //With Weyl, passes
  soak8(&g,sizeof(ee),(char*)&ee); //With Weyl, passes
 // soak8_pcg_alt(&g,sizeof(ee),(char*)&ee); //Nonsense
  //for(int r=0;r<300;r++) soak8_pcg(&g,sizeof(e),(char*)&e);

  uint32_t v=get_hash(&g);
  e++;
  fwrite(&v,sizeof(v),1,out);
 }
 fclose(out);
}
