#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#define k 0xc58efd154ce32f6d //-- original
//#define k 0x3849032c5f3a2958 //me made up
//#define k   0xaaaaaaaaaaaaaaaa //1010... -> fail
//#define k   0x5555555555555555 //0101... -> fail
//#define k   0x5555555555aa5555 //Bit mess -> ok

inline static uint32_t squares(uint64_t ctr, uint64_t key) {
   uint64_t x, y, z;
   y = x = ctr * key; z = y + key;
   x = x*x + y; x = (x>>32) | (x<<32);       /* round 1 */
   x = x*x + z; x = (x>>32) | (x<<32);       /* round 2 */
   return (x*x + y) >> 32;                   /* round 3 */
}

inline static uint32_t squares2(uint64_t ctr,uint64_t ctr2, uint64_t key) {
   uint64_t x, y, z;
   y = x = ctr * key; z = ctr2* key;
   x = x*x + y; x = (x>>32) | (x<<32);       /* round 1 */
   x = x*x + z; x = (x>>32) | (x<<32);       /* round 2 */
   return (x*x + y) >> 32;                   /* round 3 */
}

int main(int argc,char **argv){
 FILE *const out=fdopen(dup(fileno(stdout)), "wb");
 int e=0;
 int f=20;
 while(1){
  
  uint32_t v=squares(e,k);
  uint32_t u=squares(f,k);
  uint32_t uu=squares2(e,f,k);
  v=v^u;
  e++; f=(f+1)%21923;
  fwrite(&v,sizeof(v),1,out);
  fwrite(&uu,sizeof(uu),1,out);
 }
 fclose(out);
}
