#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

uint64_t splitmix(uint64_t x){
	uint64_t z = (x += 0x9e3779b97f4a7c15);
	z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
	z = (z ^ (z >> 27)) * 0x94d049bb133111eb;
	return z ^ (z >> 31);
}

int main(int argc,char **argv){
 FILE *const out=fdopen(dup(fileno(stdout)), "wb");
 int64_t e=0;
 while(1){

  uint32_t v=splitmix(e);
  e++;
  fwrite(&v,sizeof(v),1,out);
 }
 fclose(out);
}
