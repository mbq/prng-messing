#include <stdint.h>
#include <stdio.h>
#include <unistd.h>


const uint64_t K[70]={
0x42319265cc8ceb69, 0xa8ce2cbc73d9ac33,
0xc7be9af245d954e7, 0xb51352146aecf8cf,
0x40176079297a1ad0, 0xbb54c8ed0a37c1b8,
0x9ea6dab99e75637d, 0xeac6a98af9afbe78,
0x330302fc5da3e9c0, 0x4df427d54a0cd903,
0x584a8de556b8d773, 0x1f3f1ff653ac0356,
0x8eca6f2a3ebe5313, 0x9e77346f0bfe37d7,
0xc68e9af17b351cc0, 0x5c38f776b1708b56,
0xbe59c30f7cdb85a1, 0x8ed29ae2a6d7af7d,
0x41b82685f7ca3a8a, 0x834ab25d87ec97d8,
0xa96832d7f8db4123, 0xf3188b24ec1eff00,
0xe7196f576b30ce1c, 0x1ef14f16ab39c91d,
0xc326092f3bbc90bb, 0xafa377e4dd8312cb,
0xe6367815b4276901, 0xee60a9ee3608308e,
0x8c8743a77f8508c7, 0x7cd0c36df1a6d52f,
0x01ae2e93bc62207c, 0x968b9db7d5a9adf2,
0x9b541fd123733128, 0xe6a4d1d986e62c08,
0xf76d5a2bab1fb161, 0x8b1bd301c2d9b4de,
0xd5f0841fc1fbbb4f, 0xdc67c285224ccf7c,
0xe9a3f00fed0739e1, 0xe0910b92075cc803,
0x28e666bd9414ff1a, 0x6d8e9bac42a130f0,
0x70b3850f0a505a9d, 0xcc3f84c8eaa80d5e,
0x8d31b16f3c8fc89f, 0xc1d68a8b53c9d9d7,
0xe1863bf92af5f5f3, 0xe1cf89c779c80b3f,
0xcb885bcac45dcb88, 0x73a71ba11ed1cfd4,
0xf97113fc939290ec, 0x00e62b6a7ae1c5ed,
0x27cbcb1cbdc30435, 0x254429ec8c892d6b,
0xd9f7cf5b36d4fa70, 0xb6924aed71d65099,
0x89773d03e82408b4, 0x99663a74fb9fdc38,
0x6ee99bb050fa06ca, 0xfa56eed70938f3c3,
0x16c5d4fcad6eacdb, 0xb2dd9ff65874d935,
0x835b3b60916dbae5, 0xc961ba773462e4d9,
0x060a0c0c8524d325, 0x4dda10b9ae3a1403,
0x8122b607989c1145, 0xb25aa9de967d88f9,
0x3f87c09160598b9a, 0x58eb711efbd0d86b
};

inline static uint32_t mulmul(uint64_t ctr, uint64_t rounds) {
   uint64_t x=ctr;
   for(int e=0;e<rounds;e++) x=K[e]*(x+ctr+1);
 uint32_t rot=x>>59;
 uint32_t s=((x>>18)^(x))>>27;
 return (s<<((-rot)&31))|(s>>rot);
//   return x ;
}

int main(int argc,char **argv){
 FILE *const out=fdopen(dup(fileno(stdout)), "wb");
 int e=0;
 while(1){
  uint32_t v=mulmul(e,15);
  e++;
  fwrite(&v,sizeof(v),1,out);
 }
 fclose(out);
}
